//   This is a personal academic project. Dear PVS-Studio, please check it.
//
//   PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>

#ifndef CARDIOID_DIALOG_H
#define CARDIOID_DIALOG_H

#include "../include/ViewTable.h"

namespace DialogLib{
    template <typename T>
    void getObject(T& obj);

    struct DialogItem{
        const char *name;

        Table::ViewedTable *(*functionPtr)(Table::ViewedTable *tab);
    };

    class Dialog {
    private:
        const DialogItem *pItems;
        const unsigned int itemCount;

        Table::ViewedTable tab;
    public:
        Dialog(const DialogItem *pItems, const unsigned int itemCount):pItems(pItems), itemCount(itemCount){}

        void PrintMenu() const;
        [[nodiscard]] Table::ViewedTable *(*GetUserSelection() const)(Table::ViewedTable *tab);
        int Start();

        static Table::ViewedTable *Print(Table::ViewedTable *tab);
        static Table::ViewedTable *Add(Table::ViewedTable *tab);
        static Table::ViewedTable *Find(Table::ViewedTable *tab);
        static Table::ViewedTable *Remove(Table::ViewedTable *tab);
        static Table::ViewedTable *GarbageCollect(Table::ViewedTable *tab);
        static Table::ViewedTable *Exit(Table::ViewedTable *tab);
    };
}

#endif //CARDIOID_DIALOG_H
