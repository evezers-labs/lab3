//   This is a personal academic project. Dear PVS-Studio, please check it.
//
//   PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "../include/Dialog.h"

int main() {
    DialogLib::DialogItem items[] {
        "Exit", DialogLib::Dialog::Exit,
        "Print", DialogLib::Dialog::Print,
        "Add", DialogLib::Dialog::Add,
        "Find", DialogLib::Dialog::Find,
        "Remove", DialogLib::Dialog::Remove,
        "Garbage collect", DialogLib::Dialog::GarbageCollect,
    };

    DialogLib::Dialog dialog(items, sizeof(items) / sizeof(items[0]));

    return dialog.Start();
}