//   This is a personal academic project. Dear PVS-Studio, please check it.
//
//   PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com


#include <iostream>
#include <limits>

#include "../include/Dialog.h"

namespace DialogLib{
    template <typename T>
    void getObject(T &obj){
        do {
            std::cin >> obj;
            if (std::cin.fail()){
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');

                std::cerr << std::endl <<  "Incorrect input format!" << std::endl;
            } else {
                return;
            }

            if (std::cin.eof()) throw std::runtime_error("Unexpected input end");
        } while (true);
    }

    void Dialog::PrintMenu() const {
        std::cout << std::endl;
        std::cout << "MAIN MENU: " << std::endl;

        for (int i = 0; i < itemCount; ++i){
            std::cout << (char) tolower(pItems[i].name[0]) << ". " << pItems[i].name << std::endl;
        }
    }

    Table::ViewedTable *(*Dialog::GetUserSelection() const)(Table::ViewedTable *tab) {
//        unsigned int num;

        char ch;

        int index = -1;

        do {
            std::cout << "Enter menu entry: ";
            getObject(ch);
            std::cout << std::endl;

            for (int i = 0; i < itemCount; ++i){
                if (tolower(pItems[i].name[0]) == ch) index = i;
            }

            if (index < 0) std::cerr << "Enter correct menu entry" << std::endl;
        } while (index < 0);

        return pItems[index].functionPtr;
    }

    int Dialog::Start() {
        try {
            do {
                PrintMenu();
            } while (GetUserSelection()(&tab) != nullptr);
        } catch (std::runtime_error &runtimeError){
            std::cerr << runtimeError.what() << std::endl;

            Exit(&tab);

            return 1;
        }

        return 0;
    }

    Table::ViewedTable *Dialog::Print(Table::ViewedTable *tab){
        std::cout << *tab;

        return tab;
    }

    Table::ViewedTable *Dialog::Add(Table::ViewedTable *tab){
        // to switch from overloaded operator to function with prompt, uncomment following lines
//        try {
//            tab->in(std::cin, true);
//        } catch (std::runtime_error &runtimeError){
//            std::cerr << runtimeError.what() << std::endl;
//        }

//        if (tab->size() == Table::ViewedTable::maxSize) std::cerr << "Not enough space in table" << std::endl;
//        else
            getObject(*tab);

        return tab;
    }

    Table::ViewedTable *Dialog::Find(Table::ViewedTable *tabPtr){
        int key;

        std::cout << "Enter key: ";

        getObject(key);

        try {
            std::cout << (*tabPtr)[key];  // print elements from table by overloaded operators
        } catch (const std::runtime_error &ex){
            std::cerr << ex.what() << std::endl;
        }

        return tabPtr;
    }

    Table::ViewedTable *Dialog::Remove(Table::ViewedTable *tab){
        int key;

        std::cout << "Enter key: ";

        getObject(key);

        try {
            tab->remove(key);
        } catch (std::runtime_error &runtimeError){
            std::cerr << runtimeError.what() << std::endl;
        }

        return tab;
    }

    Table::ViewedTable *Dialog::GarbageCollect(Table::ViewedTable *tab){
        tab->garbageCollect();

        std::cout << "Garbage collected" << std::endl;

        return tab;
    }

    Table::ViewedTable *Dialog::Exit(Table::ViewedTable *){
        std::cout << "Good bye!\n";

        return nullptr;
    }
}
