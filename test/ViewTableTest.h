//    This is a personal academic project. Dear PVS-Studio, please check it.
//
//    PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <gtest/gtest.h>

#include "../include/ViewTable.h"

using namespace Table;

class ViewTableTest : public ::testing::Test {
    protected:
    void SetUp() override {
        t1_.add(1, "Test1");
        t1_.add(2, "Test2");
        t1_.add(3, "Test3");
        t1_.add(4, "Test4");
        t1_.add(5, "Test5");
        t1_.add(6, "Test6");
        t1_.add(7, "Test7");
        t1_.add(8, "Test8");
        t1_.add(9, "Test9");
        t1_.add(10, "Test10");

        t2_.add(0, "dfbxdb");
        t2_.add(-2, "Tesfdbxdbt1");
        t2_.add(3, "Tesdfxbxt1");
        t2_.add(34, "trbfdedf");
    }

    // void TearDown() override {}

    ViewedTable t0_;
    ViewedTable t1_;
    ViewedTable t2_;
};

