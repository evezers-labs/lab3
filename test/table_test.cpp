//    This is a personal academic project. Dear PVS-Studio, please check it.
//
//    PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <gtest/gtest.h>
#include <cstdio>

#include "ViewTableTest.h"

TEST(TableTest, ZeroInit){
    ViewedTable tab;

    EXPECT_EQ(tab.size(), 0);
    EXPECT_EQ(tab.maxSize(), 0);
}

TEST(TableTest, ElementInit){
    const char initStr[] = "Init string";
    char *str = new char[strlen(initStr) + 1];

    std::copy(initStr, initStr + strlen(initStr) + 1, str);

    TableElement el {true, 123, str};

    ViewedTable tab(&el);

    ASSERT_EQ(tab.size(), 1);
    EXPECT_EQ(tab.maxSize(), 1);
    EXPECT_STREQ(tab.getValue(123), "Init string");
    EXPECT_EQ(tab.getValue(-123), nullptr);

    TableElement el2{};

    EXPECT_NO_THROW(el2 = tab.getItem(123));

    EXPECT_EQ(el.busy, el2.busy);
    EXPECT_EQ(el.key, el2.key);
    EXPECT_STREQ(el.value, el2.value);

    EXPECT_THROW(el2 = tab.getItem(-123), std::runtime_error);

    delete[] str;
}

TEST(TableTest, ElementsInit){
    const char *initStrs[]{
        "Init string",
        "Init string2",
        "7string"
    };

    constexpr size_t elemCount = sizeof(initStrs) / sizeof(initStrs[0]);

    char *strings[elemCount];

    for (int i = 0; i < elemCount; ++i){
        strings[i] = new char[strlen(initStrs[i]) + 1];
        std::copy(initStrs[i], initStrs[i] + strlen(initStrs[i]) + 1, strings[i]);
    }


    TableElement elements[] {
        true, 123, strings[0],
        true, 546, strings[1],
        false, 7, strings[2] // it willn't copied!
    };

    int size = sizeof(elements)/sizeof(elements[0]);

    auto *tab = new ViewedTable(elements, size);

    ASSERT_EQ(tab->size(), 2);
    EXPECT_EQ(tab->maxSize(), 2);

    for (int i = 0; i < 2; ++i){
        EXPECT_STREQ(tab->getValue(elements[i].key), elements[i].value);
    }

    EXPECT_EQ(tab->getValue(7), nullptr);
    EXPECT_EQ(tab->getValue(-123), nullptr);

    delete tab; // local memory still must be accessible

    for (int i = 0; i < size; ++i){
        EXPECT_STREQ(elements[i].value, initStrs[i]);
    }

    for (int i = 0; i < elemCount; ++i){
        delete[] strings[i];
    }
}

TEST(TableTest, ArrayInit){
    int keys[] {
        3,
        5,
        8,
        9,
    };

    const char *values[]{
        "Init string",
        "Init string2",
        "Init string",
        "Init string3"
    };

    int size = sizeof(keys)/sizeof(keys[0]);

    ViewedTable tab(keys, values, size);

    ASSERT_EQ(tab.size(), size);
    EXPECT_EQ(tab.maxSize(), size);

    for (int i = 0; i < size; ++i){
        EXPECT_STREQ(tab.getValue(keys[i]), values[i]);
    }

    EXPECT_EQ(tab.getValue(-123), nullptr);

    tab.remove(9);

    auto *tab2 = new ViewedTable(tab);

    EXPECT_EQ(tab.getValue(9), nullptr);

    for (int i = 0; i < size; ++i){
        EXPECT_STREQ(tab.getValue(keys[i]), tab2->getValue(keys[i]));
    }

    for (int i = 0; i < size - 1; ++i){
        EXPECT_EQ(tab.getItem(keys[i]).busy, tab2->getItem(keys[i]).busy);
    }

    EXPECT_EQ(tab.getValue(-123), nullptr);

    delete tab2;

    //memory must be still accessible
    for (int i = 0; i < size - 1; ++i){
        EXPECT_STREQ(tab.getValue(keys[i]), values[i]);
    }
}

TEST(TableTest, ArrayConstruct){
    ViewedTable tables[2];

    EXPECT_EQ(tables[0].size(), 0);
    EXPECT_EQ(tables[1].size(), 0);
}

TEST(TableTest, AddElementTest){
    ViewedTable tab;

    EXPECT_EQ(tab.size(), 0);

    const char initStr[] = "Init string";
    char *str = new char[strlen(initStr) + 1];

    std::copy(initStr, initStr + strlen(initStr) + 1, str);

    TableElement el {true, 123, str};

    tab.add(&el);

    ASSERT_EQ(tab.size(), 1);
    EXPECT_EQ(tab.maxSize(), 1);
    EXPECT_STREQ(tab.getValue(123), "Init string");
    EXPECT_EQ(tab.getValue(-123), nullptr);

    delete [] str;
}

TEST(TableTest, AddElementsTest){
    const char *initStrs[]{
            "Init string",
            "Init string2",
            "7string"
    };

    constexpr size_t elemCount = sizeof(initStrs) / sizeof(initStrs[0]);

    char *strings[elemCount];

    for (int i = 0; i < elemCount; ++i){
        strings[i] = new char[strlen(initStrs[i]) + 1];
        std::copy(initStrs[i], initStrs[i] + strlen(initStrs[i]) + 1, strings[i]);
    }

    ViewedTable tab;

    TableElement elements[] {
            true, 123, strings[0],
            true, 546, strings[1],
            true, 7, strings[2]
    };

    int size = sizeof(elements)/sizeof(elements[0]);

    tab.add(elements, size);

    ASSERT_EQ(tab.size(), size);
    EXPECT_EQ(tab.maxSize(), size);

    for (int i = 0; i < size; ++i){
        EXPECT_STREQ(tab.getValue(elements[i].key), elements[i].value);
    }

    EXPECT_EQ(tab.getValue(-123), nullptr);

    for (int i = 0; i < elemCount; ++i){
        delete[] strings[i];
    }
}

TEST(TableTest, AddArrayTest){
    ViewedTable tab;

    int keys[] {
        3,
        5,
        8,
        9,
    };

    const char *values[]{
        "Init string",
        "Init string2",
        "Init string",
        "Init string3"
    };

    int size = sizeof(keys)/sizeof(keys[0]);

    tab.add(keys, values, size);

    ASSERT_EQ(tab.size(), size);
    EXPECT_EQ(tab.maxSize(), size);

    for (int i = 0; i < size; ++i){
        EXPECT_STREQ(tab.getValue(keys[i]), values[i]);
    }

    EXPECT_EQ(tab.getValue(-123), nullptr);
}

TEST(TableTest, AddKeyValueTest){
    ViewedTable tab;

    tab.add(34, "tredf");
    ASSERT_EQ(tab.size(), 1);
    EXPECT_EQ(tab.maxSize(), 1);

    tab.add(35, "tredf1");
    ASSERT_EQ(tab.size(), 2);
    EXPECT_EQ(tab.maxSize(), 3);

    tab.add(36, "tredf2");
    ASSERT_EQ(tab.size(), 3);
    EXPECT_EQ(tab.maxSize(), 3);

    tab.add(37, "tredf3");
    ASSERT_EQ(tab.size(), 4);
    EXPECT_EQ(tab.maxSize(), 7);

    EXPECT_STREQ(tab.getValue(34), "tredf");
    EXPECT_STREQ(tab.getValue(35), "tredf1");
    EXPECT_STREQ(tab.getValue(36), "tredf2");

    EXPECT_EQ(tab.getValue(-123), nullptr);
}


TEST_F(ViewTableTest, IsEmptyInitially) {
    EXPECT_EQ(t0_.size(), 0);
}

TEST_F(ViewTableTest, MoveConstructor){
    ViewedTable tmp(t1_);

    tmp.remove(9);

    ViewedTable t3_(std::move(tmp));

    t1_.remove(9);
    EXPECT_EQ(t3_.size(), t1_.size());

    // tmp is inaccessible
    for (int i = 0; i < t3_.size(); ++i){
        ASSERT_STREQ(t1_.getValue(i), t3_.getValue(i));
    }
}

TEST_F(ViewTableTest, MoveOperator){
    ViewedTable t3_;

    ViewedTable tmp(t1_);

    tmp.remove(9);

    t3_ = std::move(tmp);

    t1_.remove(9);
    EXPECT_EQ(t3_.size(), t1_.size());

    // tmp is inaccessible
    for (int i = 0; i < t3_.size(); ++i){
        ASSERT_STREQ(t1_.getValue(i), t3_.getValue(i));
    }
}

TEST_F(ViewTableTest, CopyOperator){
    ViewedTable t3_;

    ViewedTable tmp(t1_);

    tmp.remove(9);

    t3_ = tmp;

    t1_.remove(9);
    t1_.garbageCollect();
    EXPECT_EQ(t3_.size(), t1_.size());

    // tmp is inaccessible
    for (int i = 0; i < t3_.size(); ++i){
        ASSERT_STREQ(t1_.getValue(i), t3_.getValue(i));
    }
}

TEST_F(ViewTableTest, AddDuplicate){
    EXPECT_NO_THROW(t2_.add(1, "No Duplicate"));
    EXPECT_THROW(t2_.add(-2, "Duplicate"), std::runtime_error);
}

TEST_F(ViewTableTest, Remove){
    EXPECT_STREQ(t1_.getValue(5), "Test5");
    EXPECT_EQ(t1_.size(), 10);

    t1_.remove(5);

    EXPECT_EQ(t1_.getValue(5), nullptr);
    EXPECT_EQ(t1_.size(), 10);

    EXPECT_THROW(t1_.remove(5), std::runtime_error);
    EXPECT_THROW(t1_.remove(-123), std::runtime_error);
}

TEST_F(ViewTableTest, GarbageCollect){
    t1_.remove(1);
    t1_.remove(5);
    t1_.remove(7);

    EXPECT_EQ(t1_.size(), 10);

    t1_.garbageCollect();

    EXPECT_EQ(t1_.size(), 7);

    EXPECT_EQ(t1_.getValue(1), nullptr);
    EXPECT_STREQ(t1_.getValue(2), "Test2");
    EXPECT_STREQ(t1_.getValue(3), "Test3");
    EXPECT_STREQ(t1_.getValue(4), "Test4");
    EXPECT_EQ(t1_.getValue(5), nullptr);
    EXPECT_STREQ(t1_.getValue(6), "Test6");
    EXPECT_EQ(t1_.getValue(7), nullptr);
    EXPECT_STREQ(t1_.getValue(8), "Test8");
    EXPECT_STREQ(t1_.getValue(9), "Test9");
    EXPECT_STREQ(t1_.getValue(10), "Test10");


    EXPECT_EQ(t2_.size(), 4);

    t2_.remove(-2);
    EXPECT_EQ(t2_.getValue(-2), nullptr);

    t2_.garbageCollect();

    EXPECT_EQ(t2_.size(), 3);


    EXPECT_STREQ(t2_.getValue(0), "dfbxdb");
    EXPECT_EQ(t2_.getValue(-2), nullptr);
    EXPECT_STREQ(t2_.getValue(3), "Tesdfxbxt1");
    EXPECT_STREQ(t2_.getValue(34), "trbfdedf");
}

TEST_F(ViewTableTest, OperatorTest){
    EXPECT_STREQ(t1_[2].value, "Test2");
    EXPECT_STREQ(t1_[9].value, "Test9");

    t1_.remove(2);

    EXPECT_THROW(t1_[2], std::runtime_error);


    t1_.remove(1);
    t1_.remove(5);
    t1_.remove(7);

    EXPECT_EQ(t1_.size(), 10);

    ViewedTable new_tab = !t1_;

    EXPECT_EQ(t1_.size(), 10);
    EXPECT_EQ((!t1_).size(), 6);
    EXPECT_EQ(new_tab.size(), 6);
}
